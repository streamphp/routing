<?php

/**
 * This File is part of the Stream\Routing package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing;

/**
 * @class AbstractController
 */

abstract class AbstractController
{

}
