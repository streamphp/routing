<?php

/**
 * This File is part of the Routing\tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Routing;

use Stream\Routing\Router;
use \Mockery as m;
/**
 * @class RouterTest
 */
class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ClassName
     */
    protected $router;
    protected $container;


    protected function setUp()
    {
        if (!class_exists('Symfony\Component\Config\Resource\ResourceInterface')) {
            m::mock('alias:Symfony\Component\Config\Resource\ResourceInterface');
        }
    }
    protected function tearDown()
    {
        m::close();
    }

    public function testRouterSetup()
    {
        $router = $this->makeRouter(function ($container, $routes) {
            return new Router($container, $routes);
        });
    }

    /**
     * @test
     */
    public function testRouterBindController()
    {
        $router = $this->makeRouter(function ($container, $routes) {
            $container->shouldReceive('share')->andReturn(function () {
                return 'foo';
            });
            return new Router($container, $routes);
        });

        $router->get('/', 'MyController@showIndex');
        $router->call('/');
    }


    protected function makeRouter(\Closure $setup = null)
    {
        $container = m::mock('Stream\IoC\InterfaceContainer');
        $routes = m::mock('\Symfony\Component\Routing\RouteCollection');

        if (!is_null($setup)) {
            return $setup($container, $routes);
        }

        return new Router($container, $routes);
    }
}
