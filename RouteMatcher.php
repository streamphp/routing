<?php

/**
 * This File is part of the Stream\Routing package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouteCollection;

/**
 * @class RouteMatcher
 */
class RouteMatcher extends UrlMatcher
{
    /**
     * __construct
     *
     * @param RouteCollection $routes
     * @param RequestContext $context
     * @access public
     * @return void
     */
    public function __construct(RouteCollection $routes = null, RequestContext $context = null)
    {
        $this->routes = $routes;
        $this->context = $context;
    }

    /**
     * setRoutes
     *
     * @param RouteCollection $routes
     * @access public
     * @return mixed
     */
    public function setRoutes(RouteCollection $routes)
    {
        $this->routes = $routes;
    }
}
