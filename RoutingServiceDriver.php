<?php

/**
 * This File is part of the Stream\Routing package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing;

use Stream\Common\AbstractServiceDriver as ServiceDriver;
use Symphony\Component\Routing\RoutCollection;

/**
 * @class RoutingServiceDriver
 */

class RoutingServiceDriver extends ServiceDriver
{
    /**
     * priority
     *
     * @access public
     * @return mixed
     */
    public function priority()
    {
        return 0;
    }

    /**
     * registerServices
     *
     * @access protected
     * @return mixed
     */
    protected function registerServices()
    {
        return [
            'router' => $this->app->share(function ()
                {
                    return new Stream\Routing\Router($this->app, new RouterColletion());
                }
            )
        ];
    }
}
