<?php

/**
 * This File is part of the Stream\Routing\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing\Controller;

use Stream\IoC\InterfaceContainer;
use Stream\Routing\Router;

/**
 * Class AbstractController
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 *
 * @license MIT
 */
abstract class AbstractController implements InterfaceController
{
    /**
     * callAction
     *
     * @param InterfaceContainer $container
     * @param Router $router
     * @param mixed $arguments
     * @access public
     * @return mixed
     */
    public function callAction(InterfaceContainer $container, Router $router, $arguments)
    {
        return null;
    }
}
