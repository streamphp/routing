<?php

/**
 * This File is part of the Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Controller;

/**
 * Class: BaseController
 *
 * @uses AbstractController
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class BaseController extends AbstractController
{
}
