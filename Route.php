<?php

/**
 * This File is part of the Stream\Routing package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing;

use Symfony\Component\Routing\Route as SymfonyRoute;

/**
 * Class: Route
 *
 * @uses SymfonyRoute
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Route extends SymfonyRoute
{
}
